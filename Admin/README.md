## Build

```bash
./mvnw spring-boot:build-image -Dspring-boot.build-image.imageName=jameskolean/admin-spring-boot
```

## Publish Docker Image

```bash
docker push jameskolean/admin-spring-boot
```
If you want a specific version saved run this and replace the version
```bash
docker tag jameskolean/admin-spring-boot jameskolean/admin-spring-boot:${version}                     
docker push jameskolean/admin-spring-boot:${version}
```
note: You must login `docker login`

## Run Docker Image

```
docker run -p 8411:8411 -t jameskolean/admin-spring-boot
```